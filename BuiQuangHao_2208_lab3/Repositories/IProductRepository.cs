﻿using BuiQuangHao_2208_lab3.Models;

namespace BuiQuangHao_2208_lab3.Repositories
{
    public interface IProductRepository
    {
        Task<IEnumerable<Product>> GetAllAsync();// getallasync: danh sach cuar mot phan tu 
        Task<Product> GetByIdAsync(int id); // input id , output: product
        // lay sp theo id tra ve sp
        Task AddAsync(Product product); // add product 
        Task UpdateAsync(Product product);
        Task DeleteAsync(int id);
        // interface dung de 2 ben tuong tac duoc voi nhau 


        // sourse do contronller nam
        // 
    }
}
